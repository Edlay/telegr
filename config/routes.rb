# frozen_string_literal: true

Rails.application.routes.draw do
  # get '/chats/:title', to: 'chats#show', as: :chat
  # post '/chats', to: 'chats#create', as: :chats

  devise_for :users, controllers: { registrations: 'registrations' }
  resource :chats, only: :create
  get '/chats/:title', to: 'chats#show', as: :chat
  resource :messages, only: :create
  root 'chats#index'
end
